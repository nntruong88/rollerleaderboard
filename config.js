
var connectConfig =
{
    "connectionInfo":
    {
        "http": 
        {
            "ip": "127.0.0.1",
            "port": 8080
        },
        "database": 
        {
            "ip": "127.0.0.1",
            "port": 6379
        } 
    },
    "paths":
    {
        "addUser": "/leaderboard",
        "updateUser": "/leaderboard/:userId",
        "getUser": "/leaderboard/:userId",
        "deleteUser": "/leaderboard/:userId"
    } 
};
module.exports = {config: connectConfig};