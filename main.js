//install packges
var express = require('express');
var bodyParser = require('body-parser');
var redis = require("redis");

//Config
var {config} = require('./config');
const userHashkey = 'userId:';

//Body-parser
var urlParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();

/* CONFIG HTTP REST */
var conUserHttp = express();
conUserHttp.listen(config.connectionInfo.http.port, config.connectionInfo.http.ip, () =>{
    console.log("UsessrHTTP listening port " + config.connectionInfo.http.port);
});
conUserHttp.use(urlParser);
conUserHttp.use(jsonParser);

/* DATABASE */
var redisClient = redis.createClient(config.connectionInfo.database.port, config.connectionInfo.database.ip);
redisClient.on("error", function (err) {
    console.log(err);
});


/***** USERS ACTION */
/*
  put, post, get, delete
*/

//Add user
conUserHttp.put(config.paths.addUser, (req, res) =>
{    
    var userName = req.body.userName;
    var score = req.body.score;
    if (userName === 'undefined' || userName === null)
    {
        res.send("USERNAME IS WRONG");
    }
    else if (score === 'undefined' || score === null)
    {
        res.send("SCORE IS WRONG");
    }
    else
    {
        if(!Number.isInteger(score))
        {
            res.send("Update user is ERROR - Type must be Integer");
        }
        else
        {
            redisClient.hgetall(userName, function(userHashError, userHashValue)
            {
                if(userHashError == null && userHashValue == null)
                {
                    redisClient.hmset(userName, {"userName" : userName, "score" : score});
                    res.send('Add User is SUCCESSFUL');  
                }
                else
                {
                    res.send('User ' + userCountValue + ' have already been exists'); 
                }
            });
        }
    }
});

//Update User
conUserHttp.post(config.paths.updateUser, (req, res) =>
{
    var userId =  parseInt(req.params.userId);
    if(!Number.isInteger(userId))
    {
        res.send("Update user is ERROR - Type must be Integer");
    }
    else
    {
        if(res.err == null)
        {
            res.send("Update user is SUCCESS");        
        }
        else
        {
            res.send("Update user is ERROR - detail: " + err);
        }
    }
});

//Get user info
conUserHttp.get(config.paths.getUser, (req, res) =>
{
    var userId = parseInt(req.params.userId);
    if(!Number.isInteger(userId))
    {
        res.send("Get user info is ERROR - Type must be Integer");
    }
    else
    {
        if(res.err == null)
        {
            res.send("Get user info is SUCCESS");        
        }
        else
        {
            res.send("Get user info is ERROR - detail: " + err);
        }
    }
});

//Delete Use
conUserHttp.delete(config.paths.deleteUser, (req, res) =>
{
    var userId = parseInt(req.params.userId);
    if(!Number.isInteger(userId))
    {
        res.send("Delete user is ERROR - Type must be Integer");
    }
    else
    {
        if(res.err == null)
        {
            res.send("Delete user is SUCCESS");        
        }
        else
        {
            res.send("Delete user is ERROR - detail: " + err);
        }
    }
});
